//Gruppe 11 Mark Kisker, Zinab Amensoor, Konstantin Krabus
#include "binarytree.hpp"
#include <stdlib.h>
using namespace std;

void clearBuffer()
{
    cin.clear();
    cin.ignore(200, 'n');
} 

int main()
{
    try{
        srand(5);
        int Menge = 10;
        int abbruch = 1, eingabe, EingabeInsert, cache, EingabeLöschen, EingabeSearch;
        TreeNode<int>* tree = new TreeNode<int>(50);
        do
        {
            system("CLS");
            cout<<"\n Schwarz-Weiss Baum Menue";
            cout<<"\n 1: insert";
            cout<<"\n 2: Delete a Note from the Tree";
            cout<<"\n 3: Search a note";
            cout<<"\n 4: Display the Tree";
            cout<<"\n 5: Fill the tree with random numbers";
            cout<<"\n 6: printPreorder";
            cout<<"\n 7: printPostorder";
            cout<<"\n 8: printInorder";
            cout<<"\n 0: Exit";
            cout<<"\n Eingabe: ";
            cin>>eingabe;
            while(cin.fail())
            {
                std::cin.clear();
                std::cin.ignore(256, '\n');
                cin >>eingabe;
            }
            cout << eingabe << std::endl;
            //clearBuffer();
            try
            {
                if(eingabe == 1)
                {
                    cout<<"\n Welchen Wert soll eingefuegt werden?";
                    cin>>EingabeInsert;
                    TreeNode<int>* Keyinsert = new TreeNode<int>(EingabeInsert);
                    tree->insert(tree, Keyinsert);
                    system("Pause");
                }
                else if(eingabe == 2)
                {
                    cout<<"\n Welchen Wert soll geloescht werden?";
                    cin>>EingabeLöschen;
                    TreeNode<int>* delete_test = tree->search(tree, EingabeLöschen);
                    if (delete_test == nullptr)
                        std::cout << "Delete Fehler / Kein Baum" << std::endl;
                    else
                        tree->tree_delete(tree, delete_test);
                    system("Pause");
                }
                
                else if(eingabe == 3)
                {
                    cout<<"\n Welchen Wert soll gesucht werden?";
                    cin>>EingabeSearch;
                    TreeNode<int>* KeySearch = tree->search(tree, EingabeSearch);
                    if(KeySearch == nullptr)
                        std::cout << "Search nicht erfolgreich" << std::endl;
                    else
                        std::cout << "Search erfolgreich" << std::endl;
                    system("Pause");
                }
                else if(eingabe == 4)
                {
                    tree->print2d(tree, 0);
                    system("Pause");
                }
                else if(eingabe == 5)
                {
                    for (int i = 0; i < 10; i++){
                        cache = rand() % 100;
                        TreeNode<int>* FillTree = new TreeNode<int>(cache);
                        tree->insert(tree, FillTree);
                    }
                    system("Pause");
                }
                else if(eingabe == 6)
                {
                    tree->printPreorder(tree);
                    system("Pause");
                }
                else if(eingabe == 7)
                {
                    tree->printPostorder(tree);
                    system("Pause");
                }
                else if(eingabe == 8)
                {
                    tree->printInorder(tree);
                    system("Pause");
                }
                else if(eingabe == 0)
                {
                    cout << "\n Programm wurde beendet" << std::endl;
                    abbruch = 0;
                }
                else
                {
                    break;
                }
            }
            catch(const char* err)
            {
                cout << err << endl;
                system("Pause");
            }
        }
        while(abbruch != 0);
    }
    catch(const char* err)
    {
        cout << err << endl;
    }
    return 0;
}