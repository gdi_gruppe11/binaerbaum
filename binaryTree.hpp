//Gruppe 11 Mark Kisker, Zinab Amensoor, Konstantin Krabus
#include <iostream>
int globaldeph = 10;

using namespace std;


template<typename T>
class TreeNode {
    private:
    TreeNode* father = nullptr;
    TreeNode* left = nullptr;
    TreeNode* right = nullptr;
    T key_value;
    public:
    T get_key() {return key_value;};
    TreeNode(T key);
    TreeNode* search(TreeNode* tree, T key);
    void insert(TreeNode* tree, TreeNode *new_node);
    void inorder_tree_walk(TreeNode* tree);
    void print2d(TreeNode* tree, int deph);
    TreeNode* minimum(TreeNode* tree);
    TreeNode* maximum(TreeNode* tree);
    void transplant(TreeNode* tree, TreeNode* u, TreeNode* v);
    void tree_delete(TreeNode* tree, TreeNode* z);
    TreeNode* tree_successor(TreeNode* tree);
    void printInorder(TreeNode* tree);
    void printPreorder(TreeNode* tree);
    void printPostorder(TreeNode* tree);
    
};
//Konstruktur
//Zuweisung des Schlüssels
//Verweis auf Wurzel muss gesetzt werden
template<typename T>
TreeNode<T> :: TreeNode(T key){
    key_value = key;
    father = this;
}
//Ausgabe des Baumes wie auf Seite 291 beschrieben
template<typename T>
void TreeNode<T> :: inorder_tree_walk(TreeNode* tree){
    if(tree != nullptr){
        inorder_tree_walk(tree->left);
        std::cout << tree->key_value << std::endl;
        inorder_tree_walk(tree->right);
    }
}
//Insert Methode nach Seite 297
template<typename T>
void TreeNode<T> :: insert(TreeNode* tree, TreeNode* new_node){
    TreeNode* y = nullptr;
    TreeNode* x = tree->father;
    while (x != nullptr){
        y = x;
        if (new_node->key_value < x->key_value)
            x = x->left;
        else
            x = x->right;
    }
    new_node->father = y;
    if(y == nullptr)
        tree->father = new_node;
    else if (new_node->key_value < y->key_value)
        y->left = new_node;
    else
        y->right = new_node;
}
//Search Methode Iterativ nach Seite 293
template<typename T>
TreeNode<T>* TreeNode<T> :: search(TreeNode* tree, T key){
    while (tree != nullptr && key != tree->key_value){
        if (key < tree->key_value)
            tree = tree->left;
        else
            tree = tree->right;
    }
    return tree;
}
//Minimum Methode nach Seite 294
template <typename T>
TreeNode<T>* TreeNode<T> :: minimum(TreeNode* tree){
    while (tree->left != nullptr)
        tree = tree->left;
    return tree;
}
//Maximum Methode nach Seite 294
template <typename T>
TreeNode<T>* TreeNode<T> :: maximum(TreeNode* tree){
    while (tree->right != nullptr)
        tree = tree->right;
    return tree;
}
//Transplant nach Seite 299
template <typename T>
void TreeNode<T> :: transplant(TreeNode* tree, TreeNode* u, TreeNode* v){
    if (u->father == nullptr){
        tree->father = v;
    }
    else if (u == u->father->left){
        u->father->left = v;
    }
    else 
        u->father->right = v;
    if (v != nullptr)
        v->father = u->father;
}
//Tree-Delete nach Seite 299
template<typename T>
void TreeNode<T> :: tree_delete(TreeNode* tree, TreeNode* z){
    if (z->left == nullptr)
        tree->transplant(tree, z, z->right);
    else if (z->right == nullptr)
        tree->transplant(tree, z, z->left);
    else {
        TreeNode<T>* y = tree->minimum(z->right);
        if (y->father != z){
            tree->transplant(tree, y, y->right);
            y->right = z->right;
            y->right->father = y;
        }
        tree->transplant(tree, z, y);
        y->left = z->left;
        y->left->father = y;
    }
}
//Tree-Successor nach Seite 295
template <typename T>
TreeNode<T>* TreeNode<T> :: tree_successor(TreeNode* tree) {
    if (tree->right != nullptr)
        return minimum(tree->right);
    TreeNode* y = tree->father;
    while ((y != nullptr) && (tree == y->right)) {
        tree = y;
        y = y->father;
    }
    return y;

}

template<typename T>
void TreeNode<T> :: print2d(TreeNode* tree, int deph){
    if(tree == nullptr) return;
    deph = deph + globaldeph; 
    //std::cout << "deph: " << deph << std::endl;
    print2d(tree->right, deph);
    std::cout << std::endl;
    for(int i = globaldeph; i < deph; i++){
        std::cout << " ";
    }
    std::cout << tree->key_value << "(T:" << deph << ")" << "\n" << std::endl;
    print2d(tree->left, deph);
}

//Inorder-Traversierung
template <typename T>
void TreeNode<T> ::printInorder(TreeNode* tree) {
    if (tree == nullptr)
        return;

    /* first recur on left child */
    printInorder(tree->left);

    /* then print the data of node */
    cout << tree->key_value << " ";

    /* now recur on right child */
    printInorder(tree->right);
}
//Preorder-Traversierung
template <typename T>
void TreeNode<T> :: printPreorder(TreeNode* tree) {
    if (tree == nullptr)
        return;

    /* first print data of node */
    cout << tree->key_value << " ";

    /* then recur on left subtree */
    printPreorder(tree->left);

    /* now recur on right subtree */
    printPreorder(tree->right);
}
//Postorder-Traversierung
template <typename T>
void TreeNode<T> :: printPostorder(TreeNode* tree) {
    if (tree == nullptr)
        return;

    // first recur on left subtree
    printPostorder(tree->left);

    // then recur on right subtree
    printPostorder(tree->right);

    // now deal with the node
    cout << tree->key_value << " ";
}